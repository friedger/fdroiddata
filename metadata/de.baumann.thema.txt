Categories:Theming
License:GPLv3+
Web Site:
Source Code:https://github.com/scoute-dich/Baumann_Theme
Issue Tracker:https://github.com/scoute-dich/Baumann_Theme/issues
Changelog:https://github.com/scoute-dich/Baumann_Theme/blob/HEAD/CHANGELOG.md

Name:Blue Minimal
Auto Name:Blue Minimal      //Wallpaper
Summary:Minimalistic CM12+ theme
Description:
A Material Design inspired theme for Lollipop aiming to provide a consistent and
minimalistic look to your device. It is based on several other CM12 themes. A
device running the Cyanogenmod 12 ROM or any other rom compatible with the CM12
theme engine is needed to use the theme.

[https://github.com/scoute-dich/Baumann_Theme/blob/HEAD/screenshots.md
Screenshots]
.

Repo Type:git
Repo:https://github.com/scoute-dich/Baumann_Theme

Build:3.1,12
    commit=8c20ff6a78020b17e8b60860a5e039ecf5c3ce3d
    subdir=theme
    gradle=yes

Build:3.5,13
    commit=1e223122970457e702d76710f450b9747a582c94
    subdir=theme
    gradle=yes

Auto Update Mode:None
Update Check Mode:RepoManifest
Current Version:3.5.1
Current Version Code:14
